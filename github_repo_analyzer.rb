# frozen_string_literal: true

require 'graphql/client'
require 'graphql/client/http'
require 'graphql/client/schema'
require 'csv'

# Analyse github google repositories and their languages
class GithubRepoAnalyzer
  GITHUB_ACCESS_TOKEN = ENV['GITHUB_TOKEN']
  URL = 'https://api.github.com/graphql'
  HttpAdapter = GraphQL::Client::HTTP.new(URL) do
    def headers(_context)
      {
        'Authorization' => "Bearer #{GITHUB_ACCESS_TOKEN}",
        'User-Agent' => 'Ruby'
      }
    end
  end
  Schema = GraphQL::Client.load_schema(HttpAdapter)
  Client = GraphQL::Client.new(schema: Schema, execute: HttpAdapter)
  RepositoryQuery = Client.parse <<-'GRAPHQL'
    query($after: String) {
	  organization(login: "google"){
	    name
	    url
	    repositories(privacy: PUBLIC, first: 100, after: $after) {
	      totalCount
	      pageInfo {
	        hasNextPage
	        endCursor
	    	}
	      edges {
	        node {
	          id
	          name
	          createdAt
	          languages(first: 10)
	          {
	            edges {
	              node {
	                name
	              }
	            }
	          }
	        }
	      }
	    }
	  }
	}
  GRAPHQL

  def analyze_github_repo
    initialize_variables
    add_headers_to_csv
    while @has_next_page
      result = Client.query(RepositoryQuery, variables: { after: @end_cursor })
      @repositories = result.data.organization.repositories
      @has_next_page = @repositories.page_info.has_next_page
      @end_cursor = @repositories.page_info.end_cursor
      write_response_to_csv
    end
    fetch_languages_data
  end

  def initialize_variables
    @has_next_page = true
    @end_cursor = nil
  end

  def add_headers_to_csv
    CSV.open('repo_results.csv', 'a+') do |csv|
      csv << ['Repository Name', 'Languages', 'Repository Created At']
    end
  end

  def write_response_to_csv
    CSV.open('repo_results.csv', 'a+') do |csv|
      @repositories.edges.map(&:node).each do |node|
        languages = node.languages.edges.map(&:node).map(&:name).map(&:to_s)
        csv << [node.name, languages.join(','), node.created_at]
      end
    end
  end

  def fetch_languages_data
    csv = CSV.read('repo_results.csv', headers: true)
    languages = csv['Languages'].join(',').split(',').reject(&:empty?)
    most_languages = languages.uniq.max_by(5) { |ele| languages.count(ele) }.join(', ')
    least_languages = languages.uniq.min_by(5) { |ele| languages.count(ele) }.join(', ')
    puts "5 Most Commonly used languages are \n \n #{most_languages} \n \n
          5 Least Commonly used languages are \n \n #{least_languages} \n \n
          CSV generated Successfully!!!"
  end
end
GithubRepoAnalyzer.new.analyze_github_repo
