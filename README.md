# Github Repository Analyser

### Introduction

A program that analyzes the GitHub org “google” most commonly used and least used languages using the GitHub api(graphql). 

### Dependencies

   ```
   Ruby version - 2.6.3

   Bundler version - 2.1.4

   Rubocop version - 0.79.0
   ```

### Setup

  ```ruby
  bundle install
  ```

### Usage

  ```ruby
  ruby github_repo_analyzer.rb 
  ```